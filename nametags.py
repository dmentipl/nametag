"""
Convert name,affiliation from .csv to \confpin{name}{affiliation} for
use in nametags.tex.

To use:
    python nametags.py > tex/names-affiliation.tex

Data in 'names-affiliation.csv' should be fullname,affiliation.
"""

import csv

DATA_FILE = 'names-affiliation.csv'

with open(DATA_FILE) as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    line_count = 0
    for row in csv_reader:
        if line_count == 0:
            line_count += 1
        else:
            print(f'\\confpin{{{row[0]}}}{{{row[1]}}}')
            line_count += 1
