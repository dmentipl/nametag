nametags: names-affiliation.csv tex/conference-logo.png
	python nametags.py > tex/names-affiliation.tex
	cd tex; latexmk nametags

names-affiliation.csv:
	@echo "ERROR: names-affiliation.csv not present."

tex/conference-logo.png:
	@echo "ERROR: tex/conference-logo.png not present."

clean:
	cd tex; latexmk -c; rm -f nametags.pdf; rm -f names-affiliation.tex
