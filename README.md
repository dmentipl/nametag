Name tags
=========

Make name tags for a conference or workshop with a CSV file of names and affiliations, Python, and Latex.

To use
------

You need to add two files:

+ `names-affiliation.csv` with full names in the first column, and affiliation in the second
+ `tex/conference-logo.png` with the conference logo

You need to edit `tex/nametags.tex` with the conference name, location, and dates.

Then run make.

```
make
```

This produces a PDF file `tex/nametags.pdf` with ten name tags (2 columns of 5 rows) per A4 page.

Here is an example:

<img src="nametag-example.png" width="400"/>

### Details

The Python script takes names and affiliations from a CSV file `names-affiliation.csv`. It assumes the first column is the full name, and the second column is the affiliation. It takes the data and writes it to a tex file `tex/names-affiliation.tex` to be included in `tex/nametags.tex`.

To use it:

```
python nametags.py > tex/names-affiliation.tex
```

Then you can compile the tex:

```
cd tex
latexmk nametags
```

This should produce a PDF file `nametags.pdf` with the name tags.
